package seriaf.poo.structs;

/**
 *
 * @author Andrei
 */
public class Message {
    private final String user;
    private final String content;
    @Override
    public String toString() {
        return getUser()+":"+getContent();
    }
    public Message(String user , String content) {
        this.user = user;
        this.content=content;
    }
    private String getUser(){
        return user;
    }
    private String getContent(){
        return content;
    }
    
}
