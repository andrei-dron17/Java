package seriaf.poo.structs;
import java.util.Scanner;
/**
 *
 * @author Andrei
 */
public class Main {
    public static void main(String [] args){
        Message user;
        Scanner scanner = new Scanner( System.in );
        System.out.println("Numele de utilizator:");
        String user1 = scanner.nextLine();
        System.out.println("Introdu mesajul:");
        String message1 = scanner.nextLine();
        user=new Message(user1,message1);
        System.out.println(user.toString());
    }
}